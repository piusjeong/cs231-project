#!/usr/bin/env python
import cv2
import numpy as np
import os
import time
import argparse

HEIGHT, WIDTH =  128, 128

mouth_cascade = cv2.CascadeClassifier(
  './haarcascade_mcs_mouth.xml')

if mouth_cascade.empty():
  raise IOError('Unable to load the mouth cascade classifier xml file')

ds_factor = 1.0

def find_best_region(fqn):
  cap = cv2.VideoCapture(fqn)
  x_, y_, w_, h_ = [], [], [], []

  while True:
      ret, frame = cap.read()
      if ret is False:
        break

      frame = cv2.resize(
        frame,
        None,
        fx=ds_factor,
        fy=ds_factor,
        interpolation=cv2.INTER_AREA
        )
      gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

      mouth_rects = mouth_cascade.detectMultiScale(gray, 1.2, 40)
      for (x,y,w,h) in mouth_rects:
          x_.append(x)
          y_.append(y)
          w_.append(w)
          h_.append(h)
          break

  cap.release()
  
  return x_, y_, w_, h_

def write_mouth_video(fqn, out_file):
  """Write extracted mouth frames to a file
  
  Args:
    fqn: input file name
    out_file: output AVI filename
  """
  writer = cv2.VideoWriter(
    out_file,
    cv2.VideoWriter_fourcc(*'XVID'),
    25,
    (HEIGHT,WIDTH))

  x_, y_, w_, h_ = find_best_region(fqn)
  x = int(np.mean(x_))
  y = int(np.mean(y_))-10
  w = int(np.mean(w_))
  h = int(np.mean(h_))
  
  cap = cv2.VideoCapture(fqn)
  num_frame = 0
  
  frames = []
  while True:
      ret, frame = cap.read()
      if ret is False:
        break

      frame = cv2.resize(
        frame,
        None,
        fx=ds_factor,
        fy=ds_factor,
        interpolation=cv2.INTER_AREA
        )
      gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
  
      mouth_rects = mouth_cascade.detectMultiScale(gray, 1.2, 40)
      frame_crop = np.zeros((1,1,3))
#      cv2.rectangle(
#        img=frame,
#        pt1=(x,y),
#        pt2=(x+w,y+h),
#        color=(0,0,0),
#        thickness=1)
      frame_crop = frame[y:y+h, x:x+w, :]
    
      frame_resize = cv2.resize(frame_crop, (HEIGHT, WIDTH))
#      print("Writing frame")
      writer.write(frame_resize)
      
      frames.append(frame_resize)
  
  writer.release()
  cap.release()
  cv2.destroyAllWindows()
  
  return frames

def get_frames(fqn):
  """Return a list of frames
  """
  cap = cv2.VideoCapture(fqn)
  frames = []
  while True:
    ret, frame = cap.read()
    if ret is False:
      break
    frames.append(frame)

  frames = np.array([cv2.cvtColor(frame, cv2.COLOR_BGR2RGB) for frame in frames])
  return frames

def frames_to_groups(frames, num_frames_per_example=5):
  """
  Args:
    frames: numpy array of frames (N, H, W, C)
  """
  # correct color ordering

  frame_groups=[]
  for i in range(frames.shape[0] // num_frames_per_example):
    frame_groups.append(frames[i*num_frames_per_example:(i+1)*num_frames_per_example])
    frame_groups[-1] = np.reshape(frame_groups[-1], (-1)).astype(np.dtype('uint8'))

  return frame_groups

if __name__=="__main__":
  parser = argparse.ArgumentParser(description="Mouth detector")
  parser.add_argument("input_mpg")
  args = parser.parse_args()

  fqn=args.input_mpg
  print("Reading %s" % fqn)
  

  out_file = fqn.replace('.mpg', '.avi')

  write_mouth_video(fqn, out_file)
