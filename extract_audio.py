#!/usr/bin/env python
"""script for extracting audio
  
"""
from subprocess import call
import glob
import os

def ffmpeg_cmd(video_file, audio_file):
  """
  Args:
    video_file: input video file to extract audio from
    audio_file: output audio file 
  """
  cmd = "ffmpeg -y -i {} -vn -acodec copy {}".format(video_file, audio_file)
  call(cmd.split())

def ffmpeg_mp2_to_wav(in_audio, out_audio):
  """
  Args:
    in_audio: mp2 file
    out_audio: wav file
  """
  cmd = "ffmpeg -i {} -acodec pcm_s16le -ac 1 -ar 44100 -f wav {}".format(
      in_audio,
      out_audio
    )
  call(cmd.split())
  
if __name__=="__main__":
  output_dir = "./data/s4/"
  if not os.path.exists(output_dir):
    os.makedirs(output_dir)
  
  files = glob.glob("./data/s4/*.mpg")
  
  for file in files:
    output_fn = os.path.basename(file).replace('.mpg', '.mp2')
    out_wav = os.path.basename(file).replace('.mpg', '.wav')
    
    out_file = os.path.join(output_dir, output_fn)
    out_wav = os.path.join(output_dir, out_wav)
    
    ffmpeg_cmd(file, out_file)
    ffmpeg_mp2_to_wav(out_file, out_wav)
