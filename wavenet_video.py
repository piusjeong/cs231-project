import tensorflow as tf
import numpy as np

from wavenet import *

def reshape_video(video, num_chunks, num_frames, fs, fps, margin, target_size):
  """
  Args:
    video: an array of shape (BATCH_SIZE, TOTAL_FRAMES, HEIGHT, WIDTH, 1)
  Return:
    batch with shape (BATCH_SIZE*num_chunks, HEIGHT, WIDTH, num_frames)
  """
  num_batch, num_total_frames, height, width, _ =video.shape

  video_output = np.zeros((num_batch*num_chunks, num_frames, height, width))

  for i_batch in range(num_batch):
    for i_chunk in range(num_chunks):
      idx = i_batch*num_chunks+i_chunk
      n_start = int(margin[i_batch] + target_size*i_chunk)
#      print("i_batch:{} i_chunk:{} n_start:{}".format(i_batch, i_chunk, n_start))
      video_output[idx, :, :, :] = get_nframes(
                                      video[i_batch, :, :, :],
                                      n_start=n_start,
                                      num_frames=num_frames,
                                      fs=fs,
                                      fps=fps)
  return video_output

def get_nframes(video, n_start, num_frames, fs, fps):
  """Return frames corresponding to the audio time

  Args:
    video: array of shape (total_frames, height, width, 1)
    n_start_: audio start sample
    num_frames: number of frames to return
    fs: audio sample rate
    fps: video frames per second
  """
  # convert to timestamps
  ts = n_start / fs
  
  # convert to frame numbers
  n_frame = round(ts * fps)
#  print("     n_frame: {}".format(n_frame))
#  return video[n_frames, :, :, 0]
  return video[n_frame:n_frame+num_frames, :, :, 0]


def get_frames(video, n_start, num_samples, fs, fps):
  """Return frames corresponding to the audio time

  Args:
    video: array of shape (total_frames, height, width, 1)
    n_start_: audio start sample
    num_frames: number of frames to return
    fs: audio sample rate
    fps: video frames per second
  """
  # convert to timestamps
  ts = np.arange(n_start, n_start+num_samples) / fs
  
  # convert to frame numbers
  n_frames = np.round(ts * fps).astype(int)
#  print(n_frames)
#  print("     n_frame: {}".format(n_frame))
  return video[n_frames, :, :, 0]
  #return np.moveaxis(video[n_frame:n_frame+num_frames, :, :, 0], 0, 2)

def conv_layer_keras(x, v, num_filters, dilation_rate=1, layer=None):
  """
  Args:
    v_net: output of video encoder (batch, 128)
  """
  with tf.variable_scope("dilation-{}-{}".format(layer, dilation_rate)):
    conv1d = tf.contrib.keras.layers.Conv1D(
              filters=num_filters, kernel_size=3, padding="valid", dilation_rate=dilation_rate,
              kernel_initializer=tf.initializers.variance_scaling(2.0),
              name="conv")
    net = conv1d(x)


    v_net = tf.layers.dense(v, net.shape[1], kernel_initializer=tf.initializers.variance_scaling(2.0))
    dimension = net.shape[1]
    net = tf.tanh(net + tf.reshape(v_net, (-1, dimension, 1)))
    #net = tf.nn.leaky_relu(net)

    # gate
    conv1d_gate = tf.contrib.keras.layers.Conv1D(
              filters=num_filters, kernel_size=3, padding="valid", dilation_rate=dilation_rate,
              kernel_initializer=tf.initializers.variance_scaling(2.0),
              name="gate-conv")
    gate = conv1d_gate(x)
    v_net = tf.layers.dense(v, net.shape[1], kernel_initializer=tf.initializers.variance_scaling(2.0))
    gate = tf.sigmoid(gate + tf.reshape(v_net, (-1, dimension, 1)))

    net = net * gate

    # 1x1 kernel, x128 filters, no dilation
    skip_conv1d = tf.contrib.keras.layers.Conv1D(
              filters=num_filters, kernel_size=1, padding="same",
              kernel_initializer=tf.initializers.variance_scaling(2.0),
              name="skip-conv")

    res_conv1d = tf.contrib.keras.layers.Conv1D(
              filters=num_filters, kernel_size=1, padding="same",
              kernel_initializer=tf.initializers.variance_scaling(2.0),
              name="res-conv")

    skip = skip_conv1d(net)

    res = res_conv1d(net)
    res = res + x[:,dilation_rate:-dilation_rate,:]

  return res, skip

 
  

class WavenetVideo(object):
  def __init__(self, num_filters, num_layers, num_stacks, height, width, num_frames):
    self.variables={}
    self.num_filters = num_filters
    self.num_layers = num_layers
    self.num_stacks = num_stacks
    
    self.height = height
    self.width = width
    self.num_frames = num_frames

  def create_variables(self, N, input_size):
    """
    Args:
      N: number of samples for audio feature
    """
    self.variables['global_step'] = tf.Variable(0, trainable=False, name="global_step")
    self.variables['x_clean'] = tf.placeholder(shape=(None, N), dtype='float32', name='x_clean')
    self.variables['x_noisy'] = tf.placeholder(shape=(None, input_size, 1), dtype='float32', name='x_noisy')
    self.variables['video'] = tf.placeholder(shape=(None, self.num_frames, self.height, self.width), dtype='float32', name='video')
    return self.variables

  def create_graph(self, x_noisy, x_clean, video, target_size, mu=256, reg=1e-4, loss_type="mse"):
    """
    Args:
      x: input (u-law) 
    
    Returns:
      y_out: output (u-law)
      loss_op: loss operation
    """
    target_size = x_clean.shape[1]
    ops={}
    
    VIDEO_FILTERS_1 = 128
    VIDEO_FILTERS_2 = 64
    VIDEO_FILTERS_3 = 32
    with tf.variable_scope("video"):
     
      v_net = tf.reshape(video, (-1, self.num_frames, self.height, self.width, 1))
      for i in range(2):
        v_net = tf.layers.conv3d(v_net, filters=VIDEO_FILTERS_1, kernel_size=(3,3,3), strides=(1,2,2),
                           padding='same', kernel_initializer=tf.initializers.variance_scaling(2.0))
        v_net = tf.layers.batch_normalization(v_net)
        v_net = tf.nn.leaky_relu(v_net)
#        v_net = tf.layers.max_pooling2d(v_net, pool_size=2, strides=2)
        v_net = tf.nn.dropout(v_net, keep_prob=0.5)

      for i in range(2):
        v_net = tf.layers.conv3d(v_net, filters=VIDEO_FILTERS_2, kernel_size=(3,3,3), strides=(1,2,2),
                           padding='same', kernel_initializer=tf.initializers.variance_scaling(2.0))
        v_net = tf.layers.batch_normalization(v_net)
        v_net = tf.nn.leaky_relu(v_net)
#        v_net = tf.layers.max_pooling2d(v_net, pool_size=2, strides=2)
        v_net = tf.nn.dropout(v_net, keep_prob=0.5)

      for i in range(3):
        v_net = tf.layers.conv3d(v_net, filters=VIDEO_FILTERS_3, kernel_size=(3,3,3), strides=(1,2,2),
                           padding='same', kernel_initializer=tf.initializers.variance_scaling(2.0))
        v_net = tf.layers.batch_normalization(v_net)
        v_net = tf.nn.leaky_relu(v_net)
#        v_net = tf.layers.max_pooling2d(v_net, pool_size=2, strides=2)
        v_net = tf.nn.dropout(v_net, keep_prob=0.5)


      video_conv1 = v_net # should be 1x1x128
   
    v_net = tf.layers.flatten(v_net)
    ops['v_shape'] = tf.shape(v_net) # (-1, 2, 2, 512)
    
  #  with tf.device('/device:GPU:0'):
    with tf.name_scope("wavenet"):
      # normal convolution
      # TODO add scope here 
      
      conv_layer = tf.contrib.keras.layers.Conv1D(
                      filters=self.num_filters, kernel_size=3, padding='same',
                      kernel_initializer=tf.initializers.variance_scaling(2.0))
    
      x_noisy = conv_layer(x_noisy)
      # dialation layers
      y = []
      net = x_noisy
      for i_stack in range(self.num_stacks):
        for i_layer in range(self.num_layers):
          # TODO add scope here
          net, skip = conv_layer_keras(net, v_net, self.num_filters,
                        dilation_rate=2**(i_layer), layer=i_stack)
          # keep only the relevant portion
          index_start = (skip.shape[1]-target_size) // 2
          y.append(skip[:,index_start:index_start+target_size,:])
      
      # TODO add scope here
      y_sum = tf.stack(y, axis=0)
      y_sum = tf.reduce_mean(y_sum, axis=0) # 0.007
      ops['reduce_mean_shape'] = tf.shape(y_sum)
      
      ops['tf_stack_shape'] = tf.shape(y_sum)
      y_sum = tf.nn.leaky_relu(y_sum) # 0.25
      y_sum = tf.layers.conv1d(y_sum, filters=2048, kernel_size=3, padding='same') # 0.0027
##      y_sum = tf.batch_normalization(y_sum)
      y_sum = tf.nn.leaky_relu(y_sum)
      y_sum = tf.layers.conv1d(y_sum, filters=256, kernel_size=3, padding='same') 

      
      # dense
#      y_logits = tf.layers.conv1d(y_sum, filters=mu, kernel_size=1, padding='same', kernel_initializer=tf.initializers.variance_scaling(2.0)) # 0-255
  
  #    for v in tf.trainable_variables():
  #      print(v)
      reg_loss = tf.reduce_sum([tf.nn.l2_loss(v) for v in tf.trainable_variables()])
  
      # concat

      if loss_type=="mse":
        y_sum = tf.layers.conv1d(y_sum, filters=1, kernel_size=3, padding='same') 
        loss = tf.reduce_mean(tf.pow(x_clean-tf.squeeze(y_sum), 2), name="loss_op")
        loss_op = loss + reg*reg_loss

        y_out = y_sum
        optimizer = tf.train.AdamOptimizer(learning_rate=1e-4)
        ops['x_scaled'] = loss
        ops['x_softmax'] = loss
        ops['softmax-loss'] = loss
      else:
        y_logits = tf.layers.dense(y_sum, mu, kernel_initializer=tf.initializers.variance_scaling(2.0)) # 0-255
        # compute loss
        x_scaled = tf.cast((x_clean+1)/2.0*(mu-1), tf.uint8)
        ops['x_scaled'] = x_clean
        x_softmax = tf.one_hot(x_scaled, depth=mu, axis=-1)
        ops['x_softmax'] = x_softmax
        loss = tf.reduce_mean(
                    tf.nn.softmax_cross_entropy_with_logits_v2(
                      labels=x_softmax, logits=y_logits, name="loss_op"))
        ops['softmax-loss'] = loss
        loss_op = loss + reg*reg_loss
  
        # compute output
        y_out = (tf.cast(tf.argmax(y_logits, axis=2), dtype=tf.float32))/(mu-1)*2 - 1
  
        optimizer = tf.train.AdamOptimizer(learning_rate=1e-2, epsilon=0.1)
      train_op = optimizer.minimize(loss_op, global_step=tf.train.get_global_step())


      with tf.name_scope("summaries"):
#        tf.summary.histogram("video-conv1", video_conv1)
 #       tf.summary.histogram("video-conv2", video_conv2)
 #       tf.summary.histogram("video-conv3", video_conv3)
#        tf.summary.image('video', v_net_at_input)
        tf.summary.scalar('loss', loss)
        tf.summary.scalar('reg-loss', reg_loss)
        ops['summary-merged'] = tf.summary.merge_all()
       
      with tf.name_scope('saving'):
        self.variables['saver'] = tf.train.Saver()

    return y_out, loss_op, train_op, ops

