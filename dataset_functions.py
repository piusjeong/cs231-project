import tensorflow as tf
from config import *
import wavenet
import numpy as np
import librosa


# Prepare dataset
def _parse_function(example_proto):
    features = {
        "name":tf.FixedLenFeature((), tf.string),
        "mel":tf.FixedLenFeature((NUM_MELS*N_MELS, TSAMPLES, 1), tf.float32),
        "mel--3db":tf.FixedLenFeature((NUM_MELS*N_MELS, TSAMPLES, 1), tf.float32),
        "mel-0db":tf.FixedLenFeature((NUM_MELS*N_MELS, TSAMPLES, 1), tf.float32),
        "mel-3db":tf.FixedLenFeature((NUM_MELS*N_MELS, TSAMPLES, 1), tf.float32),
#        "mouth_video":tf.FixedLenFeature([], tf.string),
    }
    parsed_features = tf.parse_single_example(example_proto, features)
#    frames = tf.decode_raw(parsed_features['mouth_video'], tf.uint8)
#    frames = tf.reshape(frames, (5 ,HEIGHT, WIDTH, 3))
#    parsed_features["mouth_video"] = frames

    return parsed_features

def _parse_function_wavenet(example_proto):
  """
  """
  features = {
      "name":tf.FixedLenFeature((), tf.string),
      "mel":tf.FixedLenFeature((NUM_MELS*N_MELS, TSAMPLES, 1), tf.float32),
      'raw-audio':tf.FixedLenFeature([N_SAMPLES,], tf.float32),
      "raw-audio-3db":tf.FixedLenFeature([N_SAMPLES,], tf.float32),
      "raw-audio-0db":tf.FixedLenFeature([N_SAMPLES,], tf.float32),
      "raw-audio--3db":tf.FixedLenFeature([N_SAMPLES,], tf.float32),
      "mouth_video":tf.FixedLenFeature([], tf.string),
  }
  parsed_features = tf.parse_single_example(example_proto, features)

  factor = 1

  for name in ["raw-audio-3db", "raw-audio-0db", "raw-audio--3db"]:
    target, input, margin = wavenet.break_into_chunks(
              parsed_features['raw-audio'][0:N_SAMPLES // factor],
              parsed_features[name][0:N_SAMPLES // factor],
              TARGET_SIZE,
              N_SAMPLES // factor,
              NUM_LAYERS,
              NUM_STACKS)
    parsed_features[name] = tf.stack(input)
 
  parsed_features['raw-audio'] = tf.stack(target)
  parsed_features['mouth_video'] = tf.decode_raw(parsed_features['mouth_video'], tf.uint8)[0:round(128*128*3*2.96*25)]
  parsed_features['mouth_video'] = tf.reshape(parsed_features['mouth_video'], (-1, HEIGHT, WIDTH, 3))
  parsed_features['mouth_video'] = tf.image.rgb_to_grayscale(parsed_features['mouth_video'])
  parsed_features['mouth_video'] = tf.image.resize_images(
        parsed_features['mouth_video'], size=(32, 32))

  parsed_features['margin'] = tf.constant(margin)
  return parsed_features

def _parse_function_wavenet_test(example_proto):
  """
  """
  features = {
      "name":tf.FixedLenFeature((), tf.string),
      "mel":tf.FixedLenFeature((NUM_MELS*N_MELS, TSAMPLES, 1), tf.float32),
      'raw-audio':tf.FixedLenFeature([N_SAMPLES,], tf.float32),
      "raw-audio-3db":tf.FixedLenFeature([N_SAMPLES,], tf.float32),
      "raw-audio-0db":tf.FixedLenFeature([N_SAMPLES,], tf.float32),
      "raw-audio--3db":tf.FixedLenFeature([N_SAMPLES,], tf.float32),
  }
  parsed_features = tf.parse_single_example(example_proto, features)
  
  factor = 1    

  for name in ["raw-audio-3db", "raw-audio-0db", "raw-audio--3db"]:
    target, input, margin = wavenet.break_into_chunks(
              parsed_features['raw-audio'][0:N_SAMPLES // factor],
              parsed_features[name][0:N_SAMPLES // factor],
              TARGET_SIZE,
              N_SAMPLES // factor,
              NUM_LAYERS,
              NUM_STACKS)
    parsed_features[name] = tf.stack(input)

  parsed_features['raw-audio'] = tf.stack(target)
  #parsed_features['margin'] = margin
  return parsed_features

def _parse_function_wavenet_video(example_proto):
    features = {
        "raw-audio":tf.FixedLenFeature([N_SAMPLES,], tf.float32),
        "raw-audio-noisy":tf.FixedLenFeature([N_SAMPLES,], tf.float32),
        "mouth_video":tf.FixedLenFeature([], tf.string)
    }
    parsed_features = tf.parse_single_example(example_proto, features)
    parsed_features['mouth_video'] = tf.decode_raw(parsed_features['mouth_video'], tf.uint8)[0:int(128*128*3*2.96*25)]
    parsed_features['mouth_video'] = tf.image.resize_images(
        parsed_features['mouth_video'], size=(32, 32))
#    image = tf.reshape(image, (5, 128, 128, 3))
#    parsed_features['mouth_video'] = image

    return parsed_features


def my_input_fn(filenames, parse_func, batch_size):
    # Batch normalization
    # https://medium.com/@ntoyonaga/tensorflow-dataset-api-implementation-of-preprocessing-batch-normalization-174d6e0f9905
#     def _batch_normalization(parsed_features, epsilon=1e-4):    
#         tensor_in = parsed_features['mel'].values
#         mean, var = tf.nn.moments(tensor_in, axes=[0])
#         tensor_normalized=(tensor_in-mean)/(tf.sqrt(var)+epsilon)
#         parsed_features['mel-norm'] = tensor_normalized
#         parsed_features['mean'] = mean
#         parsed_features['var'] = var
#         return parsed_features
    
    dataset = tf.data.TFRecordDataset(filenames) #.take(batch_size)
    dataset = dataset.map(parse_func)
#     dataset = dataset.map(_reshape)
    dataset = dataset.shuffle(buffer_size=10000)
    dataset = dataset.batch(batch_size)
#     dataset = dataset.map(_batch_normalization)
    
    return dataset
