#!/usr/bin/env python
"""Generate tfrecords
"""
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import glob
import tensorflow as tf
import pickle

import extract_stft
import mouth_detect

import logging

import librosa
from config import *

from collections import defaultdict

import sys
import argparse

class TFRecordGenerator(object):
  """Class for generating TFRecords
  """
  def __init__(self, data_dirs, output_path, tf_record):
    """
    Args:
      data_dirs: List of data directories
      tf_record: TFRecord filename
    """
    self.data_table = None
    self.tf_record_writer = None
    self.output_path = output_path
    
    noise_files = {
#      'babble':     './data/noise/babble.wav',
#      'airport':    './data/noise/airport.wav',
#      'restaurant': './data/noise/restaurant.wav',
#      'exhibition': './data/noise/exhibition.wav',
      'street':     './data/noise/street-2.wav',
#      'car':        './data/noise/car.wav',
#      'subway':     './data/noise/subway.wav',
#      'train':      './data/noise/train.wav'
    }
 
    # signal to noise ratios
    self.snrs = [-3, 0, 3] # -3dB, 0dB, 3dB
  
    # initialize data table
    self.data_table = self.construct_data_table(data_dirs, noise_files, self.snrs)
    self.data_table.to_csv("./data_table.csv")
    
    # initialize TFRecord writer
    self.tf_record_writer = tf.python_io.TFRecordWriter(
      os.path.join(output_path, tf_record))

    # pre-load noise
    self.noise_samples = self.load_noise(noise_files)

  def construct_data_table(self, data_dirs, noise_files, snrs):
    """Construct a table of data files
    
    Args:
      snrs: SNRs to generate
    """
    logger.info("Constructing data table")
    data_table = pd.DataFrame(columns=["video", "audio", "stft"])
    for data_dir in data_dirs:
      files = glob.glob(data_dir+"/*.mpg")
    
    #==========================================================================
    # Raw data
    #==========================================================================
    # original video files
    data_table["video"] = files
    # mouth region video files 
    data_table["video-mouth"] = data_table["video"].apply(
      lambda x: x.replace('.mpg', '-mouth.avi'))
    # audio-only files
    data_table["audio"] = data_table["video"].apply(
      lambda x: x.replace('.mpg', '.wav'))
    # audio-only with noise files
    for noise_type in noise_files:
      for snr in snrs:
        data_table["audio-{}-{}".format(noise_type, snr)] = data_table["video"].apply(
          lambda x: x.replace('.mpg', '-{}-{}-noisy.wav'.format(noise_type, snr)))
    
    #==========================================================================
    # Features
    #==========================================================================
    # mouth frames
    data_table["mouth-frames"] = data_table["video"].apply(
      lambda x: x.replace('.mpg', '-mouth.frames'))
    # STFT of original audio
    data_table["stft"] = data_table["video"].apply(
      lambda x: x.replace('.mpg', '.stft'))
    # Melspectrogram of original audio
    data_table["mel"] = data_table["video"].apply(
      lambda x: x.replace('.mpg', '.mel'))
    # Melspectrogram of noisy audio
    for snr in snrs:
      data_table["mel-noisy-{}".format(snr)] = data_table["video"].apply(
        lambda x: x.replace('.mpg', '-{}-noisy.mel'.format(snr)))

#    for noise_type in noise_files:
#      data_table["mel-{}".format(noise_type)] = data_table["video"].apply(
#        lambda x: x.replace('.mpg', '-{}-noisy.mel'.format(noise_type)))

#    # Melspectrogram of original audio
#    data_table["mag"] = data_table["video"].apply(
#      lambda x: x.replace('.mpg', '.mag'))
#    # STFT of original audio in numpy format
#    data_table["stft-np"] = data_table["video"].apply(
#      lambda x: x.replace('.mpg', '.stft-np'))
#    # STFT of noisy audio
#    data_table["stft-noisy"] = data_table["video"].apply(
#      lambda x: x.replace('.mpg', '-noisy.stft'))
    return data_table

  def load_noise(self, noise_files):
    """Load noise samples into memory for faster data generation
    """
    noise_samples = {}
    for noise_type in noise_files:
      noise_file = noise_files[noise_type]
      X, sample_rate = librosa.load(noise_file)
      logger.info("Loaded {} - sample-rate={} duration={}".format(
        noise_file, sample_rate, len(X) / sample_rate))
      X = librosa.resample(X, sample_rate, FS)
      noise_samples[noise_type] = X
    return noise_samples

  def create_noisy_samples(self, X, row, snr):
    """Create samples mixed with noise

    Args:
      snr_db: signal(speeh) to noise ratio

    """
    snr_linear=10**(snr/10)
    for noise_type in self.noise_samples:
      # check if noise file exists
      audio_key = 'audio-{}-{}'.format(noise_type, snr)
      if not os.path.exists(row[audio_key]):
        # randomly sample TS chunk
        noise_sample = self.noise_samples[noise_type]
        start_index = np.random.randint(0, len(noise_sample)-len(X))
        noise_chunk = noise_sample[start_index:start_index+len(X)]
        # linearly mix noise and speech
        noisy_speech = X + noise_chunk
        speech_power = np.mean(np.abs(X)**2)
        noise_power = np.mean(np.abs(noise_chunk)**2)
        scaling_factor = (speech_power / (noise_power))
        logger.debug("scaling factor: {}".format(scaling_factor))
        noise_chunk *= np.sqrt(scaling_factor / snr_linear)
        noise_power = np.mean(np.abs(noise_chunk)**2)
        noisy_speech = X + noise_chunk

        SNR = 10*np.log10(speech_power / noise_power)
        
        logger.debug("SNR: {}".format(SNR))
        logger.info("Writing to {}".format(row[audio_key]))
        librosa.output.write_wav(row[audio_key], noisy_speech, int(FS), norm=True)


  def get_features(self, X, row, snr):
    """
    Args:
      X: clean audio samples

    Returns:
      tf.train.Feature
    """
    # load STFT feature
    if not os.path.exists(row["stft"]):
      #==========================
      # Clean audio
      #==========================
      logger.debug("Extracting STFT, mel-spectrogram, mag of audio")
      # compute STFT and melspectrogram
      stft, _, mels, mags = extract_stft.extract_stft(X)
      stft_feature = extract_stft.stft_to_feature(stft)
      logger.debug("Writing STFT")
      pickle.dump(stft_feature, open(row["stft"], "wb"))
    
      # dump melspectrogram
      mel_feature = extract_stft.mel_to_feature(np.array(mels))
      pickle.dump(mel_feature, open(row["mel"], "wb"))

      # dump magnitude
#      mag_features = []
#      for mag in mags:
#        mag_features.append(extract_stft.mel_to_feature(mag))
#      pickle.dump(mag_features, open(row["mag"], "wb"))

    if not os.path.exists(row["mel-noisy-{}".format(snr)]):
      #==========================
      # Noisy audio
      #==========================
      logger.debug("Extracting STFT, mel-spectrogram, mag of noisy samples")
      noisy_mel_features = {}
      for noise_type in self.noise_samples:
        noisy_speech = row["audio-{}-{}".format(noise_type, snr)]
        logger.debug("Loading {}".format(noisy_speech))
        X_noise, sr = librosa.load(noisy_speech, sr=int(FS))
        _, _, mels, _ = extract_stft.extract_stft(X_noise)
        mels = np.array(mels)
        noisy_mel_features[noise_type] = extract_stft.mel_to_feature(mels)
      # dump a dictionary
      pickle.dump(noisy_mel_features, open(row["mel-noisy-{}".format(snr)], "wb"))
          
    
#    logger.info("Loading STFT")
#    stft_feature = pickle.load(open(row["stft"], "rb"))
    stft_feature = None
    logger.info("Loading Melspectrogram")
    mel_features = pickle.load(open(row["mel"], "rb"))
    try:
      mag_features = pickle.load(open(row["mag"], "rb"))
    except:
      mag_features = []
    noisy_mel_features = pickle.load(open(row["mel-noisy-{}".format(snr)], "rb"))
    return stft_feature, mel_features, mag_features, noisy_mel_features

  
  def mouth_feature(self, row, num_frames_per_example=5):
    """
    Returns:
      tf.train.Feature
    """
    # load mouth feature
    if not os.path.exists(row["video-mouth"]):
      logger.info("Extracting mouth frames")
      # Write extracted mouth frames to a file
      frames = mouth_detect.write_mouth_video(
        row["video"], row["video-mouth"])
      #logger.debug([frame.shape for frame in frames])
    mouth_frames = mouth_detect.get_frames(row["video-mouth"])
    frames = np.reshape(mouth_frames, (-1)).astype(np.dtype('uint8'))

    # break up into groups of frames
    mouth_feature = tf.train.Feature(bytes_list=tf.train.BytesList(value=[frames.tobytes()]))
    return mouth_feature
  
  
  def create_example(self, row, snrs):
    """Create tf.train.Example out of a row
    """
    mouth_video_feature = self.mouth_feature(row)
    
    # load audio once
    X, sample_rate = librosa.load(row['audio'])
    X = librosa.resample(X, sample_rate, FS)

    # create noisy samples
    raw_audio_noisy_features={}
    noisy_mel_features={}
    for snr in snrs:
      self.create_noisy_samples(X, row, snr)
      audio_key = 'audio-street-{}'.format(snr)
      logger.info("Loading {}".format(row[audio_key]))
      raw_audio_noisy, _ = librosa.load(row[audio_key], int(FS))
      raw_audio_noisy_features[snr] = tf.train.Feature(float_list=tf.train.FloatList(value=raw_audio_noisy))

      _, mel_feature, _, noisy_mel_features[snr] = self.get_features(X, row, snr)

    logger.debug('X.shape: {}'.format(X.shape))
    logger.debug('raw_audio_noisy.shape: {}'.format(raw_audio_noisy.shape))
    
    # clean raw audio
    raw_audio_feature = tf.train.Feature(float_list=tf.train.FloatList(value=X))
    

#    import pdb; pdb.set_trace()
    video_basename = bytes(os.path.basename(row['video']).encode('ascii'))

    example = tf.train.Example(
      features=tf.train.Features(
        feature={
          'name':tf.train.Feature(bytes_list=tf.train.BytesList(value=[video_basename])),
          'mouth_video':mouth_video_feature, # video frames
          'mel':mel_feature, # clean
          'mel-3db':noisy_mel_features[3]['street'],
          'mel--3db':noisy_mel_features[-3]['street'],
          'mel-0db':noisy_mel_features[0]['street'],
          'raw-audio':raw_audio_feature, # clean
          'raw-audio-3db':raw_audio_noisy_features[3],
          'raw-audio--3db':raw_audio_noisy_features[-3],
          'raw-audio-0db':raw_audio_noisy_features[0]
        }))
    
    return example

  def process(self, num_to_process):
    """Iterate over self.data_table and write out to a TFRecords file
    
    Args:
      path: 
      output_path
    """
    if not os.path.exists(self.output_path):
            os.makedirs(self.output_path)

    for index, row in self.data_table.iterrows():
      if index >= num_to_process:
        break

      logger.info("%d: Writing example for %s" % (index, row["video"]))
      example = self.create_example(row, self.snrs)
      self.tf_record_writer.write(example.SerializeToString())

    self.tf_record_writer.close()

def validate(tfrecord):
  """
  """
  import tensorflow as tf
  import matplotlib.pyplot as plt
  # Prepare dataset
  def _parse_function(example_proto):
    print("example_proto:",type(example_proto))
    features = {
        'name':tf.FixedLenFeature([], tf.string),
        "mouth_video":tf.FixedLenFeature([], tf.string),
        'mel':tf.FixedLenFeature((15*N_MELS,TSAMPLES, 1), tf.float32), # clean
        'mel-3db':tf.FixedLenFeature((15*N_MELS, TSAMPLES, 1), tf.float32),
        'mel-0db':tf.FixedLenFeature((15*N_MELS, TSAMPLES, 1), tf.float32),
        'mel--3db':tf.FixedLenFeature((15*N_MELS, TSAMPLES, 1), tf.float32),

        'raw-audio':tf.FixedLenFeature([N_SAMPLES,], tf.float32),
        "raw-audio-3db":tf.FixedLenFeature([N_SAMPLES,], tf.float32),
        "raw-audio-0db":tf.FixedLenFeature([N_SAMPLES,], tf.float32),
        "raw-audio--3db":tf.FixedLenFeature([N_SAMPLES,], tf.float32),
    }
    parsed_features = tf.parse_single_example(example_proto, features)
    parsed_features['mouth_video'] = tf.decode_raw(parsed_features['mouth_video'], tf.uint8)[0:round(128*128*3*2.96*25)]

#    image = tf.decode_raw(parsed_features['mouth_video'], tf.uint8)
#    image = tf.reshape(image, (5, 128, 128, 3))
#    parsed_features['mouth_video'] = image

    return parsed_features

  training_filenames = [tfrecord]
  filenames = tf.placeholder(tf.string, shape=[None])
  dataset = tf.data.TFRecordDataset(filenames)
  dataset = dataset.map(_parse_function)
  dataset = dataset.batch(15)
  iterator = dataset.make_initializable_iterator()

  # Iterate over tfrecord - STFT
  next_element = iterator.get_next()
  
  with tf.Session() as sess:
  #     iterator = dataset_map.make_one_shot_iterator()
    sess.run(tf.global_variables_initializer())
    sess.run(iterator.initializer, feed_dict={filenames: training_filenames})
    while True:
        try:
            batch = sess.run(next_element)
            print('name', batch['name'])
            print('raw-audio.shape', batch['raw-audio'].shape)
            print('raw-audio--3db.shape', batch['raw-audio--3db'].shape)
            print('raw-audio-0db.shape', batch['raw-audio-0db'].shape)
            print('raw-audio-3db.shape', batch['raw-audio-3db'].shape)
            print('mouth_video.shape', batch['mouth_video'].shape)
            print('mel.shape', batch['mel'].shape)
            print('mel--3db.shape', batch['mel--3db'].shape)
            print('mel-0db.shape', batch['mel-0db'].shape)
            print('mel-3db.shape', batch['mel-3db'].shape)

            fig, axes = plt.subplots(4,1)
            axes[0].plot(np.reshape(batch['raw-audio'], (-1,)))
            axes[1].plot(np.reshape(batch['raw-audio--3db'], (-1,)))
            axes[2].plot(np.reshape(batch['raw-audio-0db'], (-1,)))
            axes[3].plot(np.reshape(batch['raw-audio-3db'], (-1,)))
            plt.tight_layout()

            fig, axes = plt.subplots(4,1)
            axes[0].imshow(np.hstack(np.split(batch['mel'][0,:,:,0], 15)))
            axes[1].imshow(np.hstack(np.split(batch['mel--3db'][0,:,:,0], 15)))
            axes[2].imshow(np.hstack(np.split(batch['mel-0db'][0,:,:,0], 15)))
            axes[3].imshow(np.hstack(np.split(batch['mel-3db'][0,:,:,0], 15)))
            plt.tight_layout()

            fig, axes = plt.subplots(7,11)
            mouth_video = np.reshape(batch['mouth_video'], (-1, HEIGHT, WIDTH, 3))
            idx=0
            for i in range(7):
              for j in range(11):
                if idx<74:
                  axes[i,j].imshow(mouth_video[idx,:])
                axes[i,j].set_axis_off()
                idx+=1
            plt.show()

        except tf.errors.OutOfRangeError:
            print("Out of range")
            break

if __name__=="__main__":
  # Setup command line argument parser
  parser = argparse.ArgumentParser()
  parser.add_argument('data_dir',
                      help='data directory to process')
  parser.add_argument('output_dir',
                      help='data directory to output tfrecord file')
  parser.add_argument('--num', type=int, default=1000,
                      help='number of examples to use')
  args = parser.parse_args()
  # Setup logger
  logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
  logger = logging.getLogger('TFRecordGenerator')
  logger.setLevel(logging.DEBUG)
  
  # output TFRecord file
  tf_record = os.path.basename(args.data_dir.rstrip('/')) + "_{}".format(str(args.num)) + '.tfrecord'

  logger.info("Output tfrecord: {}".format(tf_record))

  output_path = args.output_dir
  logger.debug("Creating TFRecordGenerator")
  instance = TFRecordGenerator(
    data_dirs=[args.data_dir],
    output_path=output_path,
    tf_record=tf_record)
  
  instance.process(args.num)
  
  # validate
#  validate(os.path.join(output_path, tf_record))
