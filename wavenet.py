import tensorflow as tf
import numpy as np

def get_receptive_field(target_start, target_len, num_layers=10, num_stacks=3):
  """Return receptive field indices

  Args:
    target_start: start index of target
    target_size:  length of the target
    num_layers:   number of dilation layers
    num_stack:    number of stacks

  Return:
    rfield_start: start of the receptive field
    rfield_len:   size of the receptive field
  """
  rfield_len = target_len + num_stacks * np.sum([2**(l+1) for l in range(num_layers)])
  rfield_start = target_start - (rfield_len - target_len) // 2
  return rfield_start, rfield_len

def break_into_chunks(clean_data, noisy_data, chunk_size, N, num_layers=10, num_stacks=3):
  """Reshape batch (BATCH_SIZE, 1, N, 1) into chunks

  Args:
    clean_data:  a numpy array consisting of clean target data
    noisy_data:  a numpy array consisting of noisy input data
    chunk_size:  block size (target size)
    N:           input dimension (Or how many samples to use)

  Return:
    chunks_target:  New batch containing receptive field - what to use for convolution
    chunks_input:   New batch containing target field - what to compare against
  """
  # get valid starting point
  rfield_start, rfield_len = get_receptive_field(0, chunk_size, num_layers, num_stacks)
  margin = -rfield_start

  chunks_target = []  # list of chunk_size blocks
  chunks_input = []   # list of rfield_len blocks

  for target_start in range(margin, N-2*margin, chunk_size):
    rfield_start, rfield_len = get_receptive_field(target_start, chunk_size, num_layers, num_stacks)
    chunks_target.append(clean_data[target_start:target_start+chunk_size])
    chunks_input.append(noisy_data[rfield_start:rfield_start+rfield_len])

  return chunks_target, chunks_input, margin
  #DATA_LEN=int(16000*2.96)
  #print("total data len: {}".format(DATA_LEN))
  #margin, _ = wavenet.get_receptive_field(0, DATA_LEN)
  #margin = -margin
  #print("adjusted data len: {}".format(DATA_LEN-2*margin))
  #input_start, input_size = wavenet.get_receptive_field(margin, DATA_LEN-2*margin)
  #print("input_start: {}, input_end: {}, input_size: {}".format(input_start, input_start+input_size-1, input_size))

def ulaw(x, mu=255):
  y = np.sign(x)*np.log(1 + mu*np.abs(x)) / np.log(1+mu)
  return y

def ulaw_inverse(y, mu=255):
  x = np.sign(y)*(1/mu)*((1+mu)**np.abs(y) - 1)
  return x

def conv_layer_keras(x, num_filters, dilation_rate=1, layer=None):
  with tf.variable_scope("dilation-{}-{}".format(layer, dilation_rate)):
    conv1d = tf.contrib.keras.layers.Conv1D(
              filters=num_filters, kernel_size=3, padding="valid", dilation_rate=dilation_rate,
              kernel_initializer=tf.initializers.variance_scaling(2.0),
              name="conv")
    net = conv1d(x)
    net = tf.tanh(net)
    #net = tf.nn.leaky_relu(net)

    # gate
    conv1d_gate = tf.contrib.keras.layers.Conv1D(
              filters=num_filters, kernel_size=3, padding="valid", dilation_rate=dilation_rate,
              kernel_initializer=tf.initializers.variance_scaling(2.0),
              name="gate-conv")
    gate = conv1d_gate(x)
    gate = tf.sigmoid(gate)

    net = net * gate

    # 1x1 kernel, x128 filters, no dilation
    skip_conv1d = tf.contrib.keras.layers.Conv1D(
              filters=num_filters, kernel_size=1, padding="same",
              kernel_initializer=tf.initializers.variance_scaling(2.0),
              name="skip-conv")

    res_conv1d = tf.contrib.keras.layers.Conv1D(
              filters=num_filters, kernel_size=1, padding="same",
              kernel_initializer=tf.initializers.variance_scaling(2.0),
              name="res-conv")

    skip = skip_conv1d(net)

    res = res_conv1d(net)
    res = res + x[:,dilation_rate:-dilation_rate,:]

  return res, skip

class Wavenet(object):
  def __init__(self, num_filters, num_layers, num_stacks):
    self.variables={}
    self.num_filters = num_filters
    self.num_layers = num_layers
    self.num_stacks = num_stacks

  def create_variables(self, N, input_size):
    """
    Args:
      N: number of samples for audio feature
    """
    self.variables['global_step'] = tf.Variable(0, trainable=False, name="global_step")
    self.variables['x_clean'] = tf.placeholder(shape=(None, N), dtype='float32', name='x_clean')
    self.variables['x_noisy'] = tf.placeholder(shape=(None, input_size, 1), dtype='float32', name='x_noisy')
    return self.variables

  def create_graph(self, x_noisy, x_clean, mu=256, reg=1e-4, loss_type="mse"):
    """
    Args:
      x: input (u-law) 
    
    Returns:
      y_out: output (u-law)
      loss_op: loss operation
    """
    target_size = x_clean.shape[1]
    ops={}
  #  with tf.device('/device:GPU:0'):
    with tf.name_scope("wavenet"):
      # normal convolution
      # TODO add scope here 
      
      conv_layer = tf.contrib.keras.layers.Conv1D(
                      filters=self.num_filters, kernel_size=3, padding='same',
                      kernel_initializer=tf.initializers.variance_scaling(2.0))
    
      x_noisy = conv_layer(x_noisy)
      # dialation layers
      y = []
      net = x_noisy
      for i_stack in range(self.num_stacks):
        for i_layer in range(self.num_layers):
          # TODO add scope here
          net, skip = conv_layer_keras(net, self.num_filters,
                        dilation_rate=2**(i_layer), layer=i_stack)
          # keep only the relevant portion
          index_start = (skip.shape[1]-target_size) // 2
          y.append(skip[:,index_start:index_start+target_size,:])
      
      # TODO add scope here
      y_sum = tf.stack(y, axis=0)
      ops['tf_stack_shape'] = tf.shape(y_sum)
      y_sum = tf.reduce_mean(y_sum, axis=0) # 0.007
      ops['reduce_mean_shape'] = tf.shape(y_sum)
      y_sum = tf.nn.leaky_relu(y_sum) # 0.25
      y_sum = tf.layers.conv1d(y_sum, filters=2048, kernel_size=3, padding='same') # 0.0027
##      y_sum = tf.batch_normalization(y_sum)
      y_sum = tf.nn.leaky_relu(y_sum)
      y_sum = tf.layers.conv1d(y_sum, filters=256, kernel_size=3, padding='same') 
      # dense
#      y_logits = tf.layers.conv1d(y_sum, filters=mu, kernel_size=1, padding='same', kernel_initializer=tf.initializers.variance_scaling(2.0)) # 0-255
  
  #    for v in tf.trainable_variables():
  #      print(v)
      reg_loss = tf.reduce_sum([tf.nn.l2_loss(v) for v in tf.trainable_variables()])
  
      if loss_type=="mse":
        y_sum = tf.layers.conv1d(y_sum, filters=1, kernel_size=3, padding='same') 
        loss = tf.reduce_mean(tf.pow(x_clean-tf.squeeze(y_sum), 2), name="loss_op")
        loss_op = loss + reg*reg_loss

        y_out = y_sum
        optimizer = tf.train.AdamOptimizer(learning_rate=1e-4)
        ops['x_scaled'] = loss
        ops['x_softmax'] = loss
        ops['softmax-loss'] = loss
      else:
        y_logits = tf.layers.dense(y_sum, mu, kernel_initializer=tf.initializers.variance_scaling(2.0)) # 0-255
        # compute loss
        x_scaled = tf.cast((x_clean+1)/2.0*(mu-1), tf.uint8)
        ops['x_scaled'] = x_clean
        x_softmax = tf.one_hot(x_scaled, depth=mu, axis=-1)
        ops['x_softmax'] = x_softmax
        loss = tf.reduce_mean(
                    tf.nn.softmax_cross_entropy_with_logits_v2(
                      labels=x_softmax, logits=y_logits, name="loss_op"))
        ops['softmax-loss'] = loss
        loss_op = loss + reg*reg_loss
  
        # compute output
        y_out = (tf.cast(tf.argmax(y_logits, axis=2), dtype=tf.float32))/(mu-1)*2 - 1
  
        optimizer = tf.train.AdamOptimizer(learning_rate=1e-2, epsilon=0.1)
      train_op = optimizer.minimize(loss_op, global_step=tf.train.get_global_step())


      with tf.name_scope("summaries"):
        tf.summary.scalar('loss', loss)
        tf.summary.scalar('reg-loss', reg_loss)
        ops['summary-merged'] = tf.summary.merge_all()
       
      with tf.name_scope('saving'):
        self.variables['saver'] = tf.train.Saver()

    return y_out, loss_op, train_op, ops

