#!/usr/bin/env python
"""Read audio files and generate TFRecords of stft
"""
import librosa
import librosa.display
import wave
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

import pyaudio

import audio_lib

from config import *

mel_filter, mel_inversion_filter = audio_lib.create_mel_filter(
    fft_size=1024,
    n_freq_components=N_MELS,
    start_freq=0,
    end_freq=FS/2,
    samplerate=FS)

def get_melspectrogram(X, mag, n_fft, step_size, n_mels):
  if True:
#    wav_spectrogram = audio_lib.pretty_spectrogram(
#      X.astype('float64'),
#      fft_size = n_fft, 
#      step_size = step_size, log = True, thresh = 4)
    mel_spec = audio_lib.make_mel(
      np.transpose(mag[:-1,:]), mel_filter, shorten_factor = 1)
    return mel_spec.astype(np.float64)
  else:
    mel_spec = librosa.amplitude_to_db(np.abs(librosa.feature.melspectrogram(
      X,
      sr=FS,
      n_mels=n_mels,
      hop_length=HOP_LENGTH
    )))/20.0 # divide by 20 to match output of make_mel()
  return mel_spec

def split_spec(mel, TSAMPLES = TSAMPLES):
  """Split melspectrogram into 
  """
  n_freq, t_samples =  mel.shape
  pad_size = int(np.ceil(t_samples/TSAMPLES) * TSAMPLES - t_samples)
  padding = np.ones((n_freq, pad_size))*np.min(mel)
  mel = np.concatenate((mel, padding), axis=1)
  # split into TSAMPLES-sized blocks
  mels = np.split(mel, int(t_samples+pad_size)/TSAMPLES, axis=1)
  return mels

def extract_stft(X, n_fft=1024, n_mels=N_MELS):
  """
  Args:
    X: input audio wave
  Returns:
    STFT, X, melspectrogram
  """
  stft = librosa.stft(
    X,
    n_fft=n_fft,
    hop_length=HOP_LENGTH,
    win_length=WIN_LENGTH
    )
  mag, phase = librosa.core.magphase(stft)
  mag = (librosa.power_to_db(mag**2, amin=1e-13, top_db=120, ref=np.max)/120) + 1
  mags = split_spec(mag[:-1,:])

  mel = get_melspectrogram(X, mag, n_fft, HOP_LENGTH, n_mels)
  # split up mel-spectrogram
  mels = split_spec(mel)

  print("mags", mags[0].shape, len(mags))
  print("mels", mels[0].shape, len(mels))
  """
  https://www.programcreek.com/python/example/98228/librosa.stft
  fccs = np.array(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=8).T)
  chroma = np.array(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T)
  mel = np.array(librosa.feature.melspectrogram(X, sr=sample_rate).T)
  contrast = np.array(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T)
  tonnetz = np.array(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T)
  """
  assert np.sum(stft-(np.real(stft)+np.complex(0,1)*np.imag(stft))) == 0
  return stft, X, mels, mags


def stft_to_feature(stft):
  """
  Converts stft into a tf.train.Feature object with FloatList
s  
  Concatenate real and imaginary parts
  """
  real = np.real(stft).reshape(-1)
  imag = np.imag(stft).reshape(-1)
  concat = np.concatenate((real, imag))
  return tf.train.Feature(
    float_list=tf.train.FloatList(value=concat))

def mel_to_feature(mel):
  """
  Converts mel-spectrogram into a tf.train.Feature object with FloatList
  
  Concatenate real and imaginary parts
  """
#  print(mel)
#  print(mel.shape)
  real = mel.reshape(-1)
  return tf.train.Feature(
    float_list=tf.train.FloatList(value=real))


def feature_to_stft(stft_feature):
  """Perform inverse operation of stft_to_feature(stft)
  """
  import pdb; pdb.set_trace()
  

def plot():
  wav = librosa.istft(
    stft,
    hop_length=HOP_LENGTH,
    win_length=WIN_LENGTH)
  
  librosa.output.write_wav(
    '/tmp/out.wav', wav, sr=FS)

  librosa.display.specshow(
    librosa.amplitude_to_db(np.abs(stft), ref=np.max),
    y_axis='log', x_axis='time')
  
  plt.figure()
  plt.plot(wav)
  plt.title('Recovered - ISTFT')
  
  plt.figure()
  plt.plot(X)
  plt.title('Original')
  plt.show()
  

if __name__=="__main__":
  stft, X = extract_stft("bbaf4p.wav")
  print(stft)
  feature = stft_to_feature(stft)
  example = tf.train.Example(
    features=tf.train.Features(feature={
      'stft':feature
      }))

  feature_to_stft(feature)
