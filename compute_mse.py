#!/usr/bin/env python
import librosa
import numpy as np
import glob

def compute_mse(ref, reconstructed, offset=None):
  """
  Args:
    ref:  refernce wav
    recon: reconstructed wav
  """
  ref_wav,_ = librosa.load(ref, sr=16000)
  rec_wav,_ = librosa.load(reconstructed, sr=16000)
  rec_wav=rec_wav[0:46429]
  lens = [len(ref_wav), len(rec_wav)]
  i_min = np.argmin(lens)
  offset = np.argmax(np.correlate(ref_wav, rec_wav, 'valid'))
  ref_wav_offset = ref_wav[offset:offset+46429]

  mse = np.mean((ref_wav_offset - rec_wav)**2)
  return mse

import os
from collections import defaultdict
def collect_baseline(spkr):
  files = glob.glob('data/{}/*.wav'.format(spkr))
  baseline = defaultdict(list)
  wavenet = defaultdict(list)
  wavenet_v = defaultdict(list)
  uncorrected = defaultdict(list)
  for snr in [-3, 0, 3]:
    for ref in files:
      if 'noisy' not in ref:
        wav = os.path.basename(ref).replace('.wav','')
        uncorrected[snr].append((ref, './data/{}/.wav'.format(spkr, wav, snr)))
        baseline[snr].append((ref, './outputs/baseline/{}/{}-mel-{}db.wav'.format(spkr, wav, snr)))
        wavenet[snr].append((ref, './outputs/wavenet-audio/{}/{}-raw-audio-{}db.wav'.format(spkr, wav, snr)))
        wavenet_v[snr].append((ref, './outputs/wavenet-video/{}/{}-raw-audio-{}db.wav'.format(spkr, wav, snr)))
  return baseline, wavenet, wavenet_v

def compute_mse_mp(args):
  return compute_mse(args[0], args[1])

def main():
  from multiprocessing import Pool
  r = defaultdict(lambda: defaultdict(dict))

  p = Pool(8)
  for spkr in ['s1', 's4']:
    print(spkr)
    baseline, wavenet, wavenet_v = collect_baseline(spkr)
    for snr in [-3, 0, 3]:
      print(snr)
      r[spkr]['baseline'][snr] = p.map(compute_mse_mp, baseline[snr])
      r[spkr]['wa'][snr] = p.map(compute_mse_mp, wavenet[snr])
      r[spkr]['wv'][snr] = p.map(compute_mse_mp, wavenet_v[snr])

  return r
#  for ref, rec in baseline[3]:
#    print(compute_mse(ref, rec))
#  return baseline, wavenet, wavenet_v

if __name__=="__main__":
  main()
