import numpy as np
import librosa

def ispecgram(spec,
              n_fft=512,
              hop_length=None,
              mask=True,
              log_mag=True,
              re_im=False,
              dphase=True,
              mag_only=True,
              num_iters=1000):
  """Inverse Spectrogram using librosa.
  Args:
    spec: 3-D specgram array [freqs, time, (mag_db, dphase)].
    n_fft: Size of the FFT.
    hop_length: Stride of FFT. Defaults to n_fft/2.
    mask: Reverse the mask of the phase derivative by the magnitude.
    log_mag: Use the logamplitude.
    re_im: Output Real and Imag. instead of logMag and dPhase.
    dphase: Use derivative of phase instead of phase.
    mag_only: Specgram contains no phase.
    num_iters: Number of griffin-lim iterations for mag_only.
  Returns:
    audio: 1-D array of sound samples. Peak normalized to 1.
  """
  if not hop_length:
    hop_length = n_fft // 2

  ifft_config = dict(win_length=n_fft, hop_length=hop_length, center=True)

  if mag_only:
    mag = spec[:, :, 0]
    phase_angle = np.pi * np.random.rand(*mag.shape)
  elif re_im:
    spec_real = spec[:, :, 0] + 1.j * spec[:, :, 1]
  else:
    mag, p = spec[:, :, 0], spec[:, :, 1]
    if mask and log_mag:
      p /= (mag + 1e-13 * np.random.randn(*mag.shape))
    if dphase:
      # Roll up phase
      phase_angle = np.cumsum(p * np.pi, axis=1)
    else:
      phase_angle = p * np.pi

  # Magnitudes
  if log_mag:
    mag = (mag - 1.0) * 120.0
    mag = 10**(mag / 20.0)
  phase = np.cos(phase_angle) + 1.j * np.sin(phase_angle)
  spec_real = mag * phase

  if mag_only:
    audio = griffin_lim(
        mag, phase_angle, n_fft, hop_length, num_iters=num_iters)
  else:
    audio = librosa.core.istft(spec_real, **ifft_config)
  return np.squeeze(audio / audio.max())

def griffin_lim(mag, phase_angle, n_fft, hop, num_iters):
  """Iterative algorithm for phase retrival from a magnitude spectrogram.
  Args:
    mag: Magnitude spectrogram.
    phase_angle: Initial condition for phase.
    n_fft: Size of the FFT.
    hop: Stride of FFT. Defaults to n_fft/2.
    num_iters: Griffin-Lim iterations to perform.
  Returns:
    audio: 1-D array of float32 sound samples.
  """
  fft_config = dict(n_fft=n_fft, win_length=n_fft, hop_length=hop, center=True)
  ifft_config = dict(win_length=n_fft, hop_length=hop, center=True)
  complex_specgram = inv_magphase(mag, phase_angle)
  for i in range(num_iters):
    audio = librosa.istft(complex_specgram, **ifft_config)
    if i != num_iters - 1:
      complex_specgram = librosa.stft(audio, **fft_config)
      _, phase = librosa.magphase(complex_specgram)
      phase_angle = np.angle(phase)
      complex_specgram = inv_magphase(mag, phase_angle)
  return audio

def inv_magphase(mag, phase_angle):
  phase = np.cos(phase_angle) + 1.j * np.sin(phase_angle)
  return mag * phase


if __name__=="__main__":
  import matplotlib.pyplot as plt
  # original STFT
  file = 'data/s1/swwv9a.stft'
  stft = np.load('data/s1/swwv9a.stft-np.npy')
  mag, phase = librosa.core.magphase(stft)
  mag = (librosa.power_to_db(mag**2, amin=1e-13, top_db=120, ref=np.max)/120) + 1
  # stft = pickle.load(open(file_stft, 'rb'))
  # stft = np.reshape(stft.float_list.value, (2, -1))
  # real = stft[0,:]
  # imag = stft[1,:]
  # STFT = np.log(np.reshape(np.abs(real+np.complex(0,1)*imag), (513, -1)))
  print(mag.shape)
  fig, ax = plt.subplots(nrows=1,ncols=1, figsize=(20,4))
  plt.imshow(mag)
  plt.show()
  # cax = ax.matshow(STFT, cmap=plt.cm.afmhot, origin='lower', aspect='auto',interpolation='nearest')
  # x = librosa.istft(10**STFT, hop_length=160, win_length=640)
  x = ispecgram(np.expand_dims(mag, 3), 1024, hop_length=160, num_iters=100)
  # x = audio_lib.invert_pretty_spectrogram(
  #     np.transpose(STFT),
  #     fft_size=1024,
  #     step_size=160,
  #     n_iter = 1000)

  plt.figure()
  plt.plot(x)
  plt.show()

  plt.figure()
  plt.plot(np.reshape(mag, (-1)))
  plt.show()
  librosa.output.write_wav('/tmp/orig.wav', x, sr=16000, norm=True)
