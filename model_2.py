import tensorflow as tf
from config import *

"""
2018/5/15
First successful denoising model - 
Overfit on 100 examples of s1 
Tensorboard - 2018_May_15_03:27:37
Stored checkpoint - 0515_model-1
"""

def relu(net, leaky=True):
  if leaky:
    return tf.nn.leaky_relu(net, name="leaky_relu")
  return tf.nn.relu(net, name="relu")

def nsynth_baseline(features, labels, mode, params):
    FACTOR=1
    #with tf.device('/cpu:0'):
    with tf.device('/device:GPU:0'):
        with tf.variable_scope("video"):
          v_net = tf.feature_column.input_layer(
                    features, params['video_feature_columns'])
          v_net = tf.reshape(v_net, (-1, NUM_FRAMES, HEIGHT, WIDTH, 3))
          for i in range(2):
            v_net = tf.layers.conv3d(v_net, filters=int(128/FACTOR), kernel_size=(3,5,5),
                               padding='same')
            v_net = tf.layers.batch_normalization(v_net)
            v_net = tf.nn.leaky_relu(v_net)
            v_net = tf.layers.max_pooling3d(v_net, pool_size=(1,2,2), strides=(2,2,2))
            v_net = tf.nn.dropout(v_net, keep_prob=0.5)
        
          for i in range(2):
            v_net = tf.layers.conv3d(v_net, filters=int(256/FACTOR), kernel_size=(3,3,3),
                               padding='same')
            v_net = tf.layers.batch_normalization(v_net)
            v_net = tf.nn.leaky_relu(v_net)
            v_net = tf.layers.max_pooling3d(v_net, pool_size=(1,2,2), strides=(2,2,2))
            v_net = tf.nn.dropout(v_net, keep_prob=0.5)
        
          for i in range(2):
            v_net = tf.layers.conv3d(v_net, filters=int(512/FACTOR), kernel_size=(3,3,3),
                               padding='same')
            v_net = tf.layers.batch_normalization(v_net)
            v_net = tf.nn.leaky_relu(v_net)
            v_net = tf.layers.max_pooling3d(v_net, pool_size=(1,2,2), strides=(1,2,2))
            v_net = tf.nn.dropout(v_net, keep_prob=0.5)
    
#        v_net = tf.reshape(v_net, (-1, 2*2*NUM_FRAMES*3))
        v_shape = tf.shape(v_net)
      
        net = tf.feature_column.input_layer(features, params['feature_columns'])
        net = tf.reshape(net, (-1, N_MELS, AUDIO_TEMPORAL_SAMPLES, 1))
        
        with tf.variable_scope('audio') as scope:
          with tf.variable_scope('conv1') as scope:
            w = tf.get_variable('weights', [5, 5, 1, 128/FACTOR], initializer=tf.variance_scaling_initializer(2.0))
            b = tf.get_variable('biases', [128/FACTOR])
            net = tf.nn.conv2d(net, w, strides=[1, 2, 2, 1], padding='SAME') + b
  #        net = tf.layers.conv2d(net, filters=int(128/FACTOR), kernel_size=(5,5), strides=(2,2),
  #                               padding='same')
            net = tf.layers.batch_normalization(net)
            net = relu(net) #tf.nn.leaky_relu(net)
  
          for i in range(1):
              net = tf.layers.conv2d(net, filters=int(128/FACTOR), kernel_size=(4,4), strides=(2,2),
                                     padding='same', kernel_initializer=tf.variance_scaling_initializer(2.0))
              net = tf.layers.batch_normalization(net)
              net = relu(net) #tf.nn.leaky_relu(net)
  
          for i in range(1):
              net = tf.layers.conv2d(net, filters=int(256/FACTOR), kernel_size=(4,4), strides=(2,1),
                                     padding='same', kernel_initializer=tf.variance_scaling_initializer(2.0))
              net = tf.layers.batch_normalization(net)
              net = relu(net) #tf.nn.leaky_relu(net)
  
          for i in range(1):
              net = tf.layers.conv2d(net, filters=int(512/FACTOR), kernel_size=(4,4), strides=(2,1),
                                     padding='same', kernel_initializer=tf.variance_scaling_initializer(2.0))
              net = tf.layers.batch_normalization(net)
              net = relu(net) #tf.nn.leaky_relu(net)
  
          net = tf.layers.conv2d(net, filters=int(512/FACTOR), kernel_size=(4,4), strides=(1,1),
                                 padding='same', kernel_initializer=tf.variance_scaling_initializer(2.0))
          net = tf.layers.batch_normalization(net)
          net = relu(net) #tf.nn.leaky_relu(net)
  
          net = tf.layers.conv2d(net, filters=int(1024/FACTOR), kernel_size=(1,1), strides=(1,1),
                                 padding='same', kernel_initializer=tf.variance_scaling_initializer(2.0))
          net = tf.layers.batch_normalization(net)
          net = relu(net) #tf.nn.leaky_relu(net)
  
          # z
          net = tf.layers.conv2d(net, filters=1984, kernel_size=(1,1), strides=(1,1),
                                 padding='same', kernel_initializer=tf.variance_scaling_initializer(2.0))
          z_shape = tf.shape(net)
          net = tf.layers.batch_normalization(net)
  
          # concatenate
          #net = tf.layers.flatten(net)
          #v_net = tf.layers.flatten(v_net)
  #        net = tf.reshape(net, (-1, 5*5*64)) # 1600
  #        v_net = tf.reshape(net, (-1, 2*2*256)) # 1024
  #        net = tf.concat([net, v_net], axis=-1) # 2624
  
          # dense
          #net = tf.layers.dense(net, units=1312)
          #net = tf.layers.dense(net, units=1600)
          #net = tf.reshape(net, (-1, 5, 5, 64))
  
          # decode
          net = tf.layers.conv2d_transpose(net, filters=int(1024/FACTOR), kernel_size=(1,1), strides=(1,1),
                                 padding='same', kernel_initializer=tf.variance_scaling_initializer(2.0))
          net = tf.layers.batch_normalization(net)
          net = relu(net) #tf.nn.leaky_relu(net)
  
          for i in range(1):
              net = tf.layers.conv2d_transpose(net, filters=int(512/FACTOR), kernel_size=(4,4), strides=(2,1),
                                     padding='same', kernel_initializer=tf.variance_scaling_initializer(2.0))
              net = tf.layers.batch_normalization(net)
              net = relu(net) #tf.nn.leaky_relu(net)
  
          for i in range(1):
              net = tf.layers.conv2d_transpose(net, filters=int(256/FACTOR), kernel_size=(4,4), strides=(2,1),
                                     padding='same', kernel_initializer=tf.variance_scaling_initializer(2.0))
              net = tf.layers.batch_normalization(net)
              net = relu(net) #tf.nn.leaky_relu(net)
  
          for i in range(1):    
              net = tf.layers.conv2d_transpose(net, filters=int(128/FACTOR), kernel_size=(4,4), strides=(2,2),
                                 padding='same', kernel_initializer=tf.variance_scaling_initializer(2.0))
              net = tf.layers.batch_normalization(net)
              net = relu(net) #tf.nn.leaky_relu(net)
  
          net = tf.layers.conv2d_transpose(net, filters=int(128/FACTOR), kernel_size=(5,5), strides=(2,2),
                                 padding='same', kernel_initializer=tf.variance_scaling_initializer(2.0))
          net = tf.layers.batch_normalization(net)
          net = relu(net) #tf.nn.leaky_relu(net)
  
          net = tf.layers.conv2d_transpose(net, filters=1, kernel_size=(5,5), strides=(1,1),
                                 padding='same', kernel_initializer=tf.variance_scaling_initializer(2.0))
          net = tf.layers.batch_normalization(net)
          net = relu(net) #tf.nn.leaky_relu(net)
  
          net = tf.layers.conv2d_transpose(net, filters=1, kernel_size=(1,1), strides=(1,1),
                                 padding='same', kernel_initializer=tf.variance_scaling_initializer(2.0))
        pred = net
#         pred = tf.nn.sigmoid(net)
    
    return pred, {'z_shape':z_shape}
