NUM_LAYERS=9
NUM_STACKS=1
NUM_FILTERS=80

FS=16e3
N_FFT=1024
N_SAMPLES=47648
MU=256
TARGET_SIZE=1601

NUM_MELS = 15
WIN_LENGTH=640
HOP_LENGTH=160
N_MELS=80
NUM_FREQ_BINS=512
AUDIO_TEMPORAL_SAMPLES=20 # 20 STFT windows
TSAMPLES=AUDIO_TEMPORAL_SAMPLES

# Video 
NUM_FRAMES=3 # number of frames to use for 1601 samples
WIDTH=128
HEIGHT=128
FPS=25

# Duration of each speech file
TS=3 # sec

# Duration of noise samples
TS_NOISE=10 # sec


