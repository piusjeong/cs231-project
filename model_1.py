import tensorflow as tf
from config import *

"""
2018/5/15
First successful denoising model - 
Overfit on 100 examples of s1 
Tensorboard - 2018_May_15_03:27:37
Stored checkpoint - 0515_model-1
"""
def nsynth_baseline(features, labels, mode, params):
    with tf.device('/cpu:0'):
#    with tf.device('/device:GPU:0'):
        FACTOR=2
        net = tf.feature_column.input_layer(features, params['feature_columns'])
        net = tf.reshape(net, (-1, N_MELS, AUDIO_TEMPORAL_SAMPLES, 1))
        net = tf.layers.conv2d(net, filters=int(128/FACTOR), kernel_size=(5,5), strides=(1,1),
                               padding='same')
        net = tf.layers.batch_normalization(net)
        net = tf.nn.leaky_relu(net)

        for i in range(1):
            net = tf.layers.conv2d(net, filters=int(128/FACTOR), kernel_size=(4,4), strides=(1,1),
                                   padding='same')
            net = tf.layers.batch_normalization(net)
            net = tf.nn.leaky_relu(net)

        for i in range(1):
            net = tf.layers.conv2d(net, filters=int(256/FACTOR), kernel_size=(4,4), strides=(1,1),
                                   padding='same')
            net = tf.layers.batch_normalization(net)
            net = tf.nn.leaky_relu(net)

        for i in range(1):
            net = tf.layers.conv2d(net, filters=int(512/FACTOR), kernel_size=(4,4), strides=(1,1),
                                   padding='same')
            net = tf.layers.batch_normalization(net)
            net = tf.nn.leaky_relu(net)

        net = tf.layers.conv2d(net, filters=int(512/FACTOR), kernel_size=(4,4), strides=(1,1),
                               padding='same')
        net = tf.layers.batch_normalization(net)
        net = tf.nn.leaky_relu(net)

        net = tf.layers.conv2d(net, filters=int(1024/FACTOR), kernel_size=(1,1), strides=(1,1),
                               padding='same')
        net = tf.layers.batch_normalization(net)
        net = tf.nn.leaky_relu(net)

        # z
        net = tf.layers.conv2d(net, filters=1984, kernel_size=(1,1), strides=(1,1),
                               padding='same')
        net = tf.layers.batch_normalization(net)

        # decode
        net = tf.layers.conv2d_transpose(net, filters=int(1024/FACTOR), kernel_size=(1,1), strides=(1,1),
                               padding='same')
        net = tf.layers.batch_normalization(net)
        net = tf.nn.leaky_relu(net)

        for i in range(1):
            net = tf.layers.conv2d_transpose(net, filters=int(512/FACTOR), kernel_size=(4,4), strides=(1,1),
                                   padding='same')
            net = tf.layers.batch_normalization(net)
            net = tf.nn.leaky_relu(net)

        for i in range(1):
            net = tf.layers.conv2d_transpose(net, filters=int(256/FACTOR), kernel_size=(4,4), strides=(1,1),
                                   padding='same')
            net = tf.layers.batch_normalization(net)
            net = tf.nn.leaky_relu(net)

        for i in range(1):    
            net = tf.layers.conv2d_transpose(net, filters=int(128/FACTOR), kernel_size=(4,4), strides=(1,1),
                               padding='same')
            net = tf.layers.batch_normalization(net)
            net = tf.nn.leaky_relu(net)

        net = tf.layers.conv2d_transpose(net, filters=int(128/FACTOR), kernel_size=(5,5), strides=(1,1),
                               padding='same')
        net = tf.layers.batch_normalization(net)
        net = tf.nn.leaky_relu(net)

        net = tf.layers.conv2d_transpose(net, filters=1, kernel_size=(5,5), strides=(1,1),
                               padding='same')
        net = tf.layers.batch_normalization(net)
        net = tf.nn.leaky_relu(net)

        net = tf.layers.conv2d_transpose(net, filters=1, kernel_size=(1,1), strides=(1,1),
                               padding='same')
        pred = net
#         pred = tf.nn.sigmoid(net)
    
    return pred
