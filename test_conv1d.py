import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from wavenet import Wavenet, ulaw

np.random.seed(0)
np.random.seed(1)

N = 1600
Fs= 160
mu=256

tf.reset_default_graph()
# training
y=tf.placeholder(shape=(None, N, 1), dtype='float32', name='y')

# validation
val_x=tf.placeholder(shape=(1, N, 1), dtype='float32')
val_x_noisy=tf.placeholder(shape=(1, N, 1), dtype='float32')
val_y=tf.placeholder(shape=(1, N, 1), dtype='float32')

wavenet = Wavenet()
variables = wavenet.create_variables(N=N)
y_out, loss_op, train_op, ops = wavenet.create_graph(
  variables['x_noisy'], variables['x_clean'], mu, num_stack=1, reg=1e-3, loss_type="softmax")


#y = tf.multiply(x, 2)
BATCH_N=10
noise = np.reshape(0.1*np.random.randn(BATCH_N,N), (BATCH_N,N,1))
input_x_clean = np.zeros((BATCH_N, N))
for i in range(BATCH_N):
  t = np.arange(N)/(Fs/((i+1)*0.1))
  input_x_clean[i,:] = np.reshape(np.sin(2*np.pi*t), (1,N))
print(input_x_clean.shape)
input_x_noisy = ulaw(np.reshape(input_x_clean, (BATCH_N, N, 1))) + noise
print(input_x_noisy.shape)
# validation input
phase = np.pi/4
freq_factor=0.723
val_noise = np.reshape(0.5*np.random.randn(N), (1,N,1))
val_input_x_noisy = np.reshape(np.sin(2*np.pi*t*freq_factor+phase) , (1,N,1))
val_input_x_noisy = ulaw(val_input_x_noisy) + val_noise
val_input_x = np.reshape(np.sin(2*np.pi*t*freq_factor+phase), (1,N))
val_input_x = ulaw(val_input_x)

with tf.Session() as sess:

  sess.run(tf.global_variables_initializer())
 
  for i in range(100000):
    feed_dict = {
        variables['x_noisy']: input_x_noisy,
        variables['x_clean']: input_x_clean
      }
    output_y, train_out, loss = sess.run([
      y_out, train_op, loss_op], feed_dict=feed_dict)

    if i % 1000 == 0:
      print(i, loss)
    if i % 10000 == 0:
      output_y = np.reshape(output_y, (-1))
      plt.plot(np.reshape(input_x_clean, (-1)), label='input_x_clean')
      plt.plot(np.reshape(input_x_noisy, (-1)), label='input_x_noisy')
      plt.plot(np.reshape(output_y, (-1)), label='output_y')
      #print("output_y: {}".format(output_y))
      #print("train_out", train_out)
      print("loss_op", loss)

      plt.legend()
      # plot validation
      feed_dict = {
              variables['x_noisy']: val_input_x_noisy,
              variables['x_clean']: val_input_x
            }
      val_output_y, val_loss = sess.run(
        [y_out, loss_op], feed_dict=feed_dict)

     
      
      plt.figure()
      plt.plot(np.reshape(val_input_x_noisy, (-1)), label='val_input_x_noisy')
      plt.plot(np.reshape(val_input_x, (-1)), label='val_input_x')
      plt.plot(np.reshape(val_output_y, (-1)), label='val_output_y')
      print("val_loss_op", val_loss)
      

      plt.legend()
      plt.show()
  


